# Behind the scenes of the interactive Tauc plot evaluator

The Tauc fitting algorithm in `uvvistauc::tauc()` can be fragile and return
errors if the input parameters don't make sense for the UV-Vis spectrum
in question.

In the first iteration of this reactive app (which immediately called `tauc()`
with whatever parameters were set in the app), this frequently led to the user
being met by an irrelevant error message in red text instead of a plot.

I [considered using `shinyglide`](https://appsilon.com/generating-word-documents-from-table-data-in-shiny/)
to achieve the same effect, but I think hiding/showing (or otherwise drawing over)
the plot is simpler and I'm happy that this approach worked out.

With the current implementation,  `observeEvent(input$file1, {...})` combined with
`observeEvent(input$buttonTauc, {...})`, when uploading new data in the same
session the previous Tauc plot re-appears briefly before being replaced by
the correct one. I can live with this, but ideally we should address this issue.


## Links and notes

+ https://stackoverflow.com/questions/23233497/outputting-multiple-lines-of-text-with-rendertext-in-r-shiny
+ https://shiny.rstudio.com/reference/shiny/latest/textoutput
+ https://shiny.rstudio.com/reference/shiny/latest/fileinput
+ https://gist.github.com/nutterb/7d723f64201c9afa8bb57774b47bfd7c
+ https://stackoverflow.com/questions/20333399/are-there-global-variables-in-r-shiny
+ https://stackoverflow.com/questions/30294928/r-shiny-how-to-share-variables-between-rendering-functions
+ https://bookdown.org/yihui/rmarkdown/
+ https://plotly-r.com/overview.html
+ https://shiny.rstudio.com/tutorial/
