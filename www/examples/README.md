# Example spectra

Short description of the included example spectra.
If you add more spectra, please add a description below.


## Fe2O3-thin-film-ZnO_r=0.5_3.1-3.3_0.48-0.85_1.5-1.75.csv

Thin film of iron oxide (most likely poorly crystallised hematite) on top
of ZnO nanorods.

![](https://codeberg.org/solarchemist/uvvistauc-reactive/raw/branch/main/www/screenshots/Fe2O3-thin-film-ZnO_r=0.5_3.1-3.3_0.48-0.85_1.5-1.75.png)


## ZnO-QDs-MB_r=2_3.6-3.9_0.25-0.70_2.8-3.1.txt

ZnO quantum dots in aqueous suspension, in the presence of methylene blue.
The band edge is distinct and not obscured in any way.

![](https://codeberg.org/solarchemist/uvvistauc-reactive/raw/branch/main/www/screenshots/ZnO-QDs-MB_r=2_3.6-3.9_0.25-0.70_2.8-3.1.png)


## ZnO-QDs-MB_r=2_3.75-4.00_0.20-0.75_2.75-3.15.txt

ZnO quantum dots in aqueous suspension, in the presence of methylene blue.
This spectrum shows a slightly elevated baseline (due to scattering in the solution) but since the band edge is mostly vertical
this leads to only a very small artificial shift of the calculated optical band gap.

![](https://codeberg.org/solarchemist/uvvistauc-reactive/raw/branch/main/www/screenshots/ZnO-QDs-MB_r=2_3.75-4.00_0.20-0.75_2.75-3.15.png)


## ZnO-QDs-MB_r=2_4.0-4.2_0.20-0.50_2.8-3.2.txt

ZnO quantum dots in aqueous suspension. This spectrum demonstrates significant baseline shift due to strong scattering in the solution
(in turn due to high turbidity). This leads to a large artifical shift of the calculated band gap (this is effectively an error,
caused by the poor transparency of the solution).

![](https://codeberg.org/solarchemist/uvvistauc-reactive/raw/branch/main/www/screenshots/ZnO-QDs-MB_r=2_4.0-4.2_0.20-0.50_2.8-3.2.png)
